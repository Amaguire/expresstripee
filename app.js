var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var routes = require('./api/routes');
var Amadeus = require('amadeus');
var bodyParser = require('body-parser');
var parseJson = require('parse-json');
var objectToJson = require('object-to-json');
var nodefilter = require('node-json-filter');
var destinationList = require('dest-list-tripee');
var delay = require('delay');
var await = require('await');
var origin = require('tripee-origins');
var flightRoutes = require('routes-tripee');
var fs = require('fs');

if (typeof localStorage === "undefined" || localStorage === null) {
	  var LocalStorage = require('node-localstorage').LocalStorage;
	  localStorage = new LocalStorage('./scratch');
	}


// connects to local database flightroutes
var mysql = require('mysql');
 var con = mysql.createConnection({
	  host: "localhost",
	  user: "root",
	  password:"password",
	  database: "flightroutes"} );
 
 var db;
 
 var mongoose = require('mongoose');
 mongoose.promise= global.Promise; 
 mongoose.connect('mongodb://localhost/Tripee',  { useNewUrlParser: true },  function (err, database) {
	    if (err) throw err;
	    else { 
		   db= database;
	   console.log('Successfully connected to mongodb');
	   }
	});
   
  
var app = express();
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ extended: false })); // to support
														// URL-encoded bodies
/*
//test amadeus 
 var amadeus = new Amadeus({
	// hostname: 'production',
    clientId:    'S197I2kaGs8XDgyvx7wPv9kwpShtvGi2',
     clientSecret: 'ipCGQBatkmgARpHp'
 });

*/

 
var OrigOne;
var OrigTwo;

var destOne;
var destTwo;

var DesListOne;
var DesListTwo;

var iataCode1;
var iataCode2;



 app.post('/', function getOrigins(req,res){
		// console.log('request received');
		// console.log(JSON.stringify(req.body)) ;
	
	 //localStorage.clear();
		res.status(200);
		res.send({ hello: 'world' });
		
		var parsed1 = req.body.originOne;
		var parsed2 = req.body.originTwo;
		
		//OrigOne = 
		var split1 = parsed1.split(',');
	
		var split2 = parsed2.split(',');
		console.log(split1[0]);
		console.log(split2[0]);
		
		OrigOne= split1[0];
		OrigTwo = split2[0];
		
	
		
		console.log('THis is setting the values : '+ OrigOne + ' And '+ OrigTwo);
		 
		IataCodeSearchOrg1(res, OrigOne, OrigTwo);
		//firstOrigin(res, OrigOne);
		
		//console.log(res2);
	
		
 function IataCodeSearchOrg1(res, OrigOne, OrigTwo){
	 
	 amadeus.referenceData.locations.get({
		
		keyword: OrigOne,
		subType: Amadeus.location.any
		 
	 }).then(function (response){
		 
		 console.log(response.data[0].iataCode);
		 
		 iataCode1 = response.data[0].iataCode;
		 
		origin.setFirstOrigin(iataCode1);
			
	 IataCodeSearchOrg2(res, OrigTwo, iataCode1);
	 
	 }).catch(function(error){
		  console.log(error.response); // => The response object with
			// (un)parsed data
		  console.log(error.response.request); // => The details of
					// the// request// made
		  console.log(error.code); // => A unique error code to identify //
	// the=// type of error

})
 }
 
 function IataCodeSearchOrg2(res, OrigTwo, iataCode1){
	 
	 amadeus.referenceData.locations.get({
		
		keyword: OrigTwo,
		subType: Amadeus.location.any
		 
	 }).then(function (response){
		 console.log(response.data[0].iataCode);
		 
		 iataCode2= response.data[0].iataCode;
		 
		origin.setSecondOrigin(iataCode2);
		
		firstOrigin(res, iataCode1, iataCode2);
	 }).catch(function(error){
		  console.log(error.response); // => The response object with
			// (un)parsed data
		  console.log(error.response.request); // => The details of
					// the// request// made
		  console.log(error.code); // => A unique error code to identify //
	// the=// type of error

})
	 
 }
 
 
 
	function firstOrigin(res, iataCode1, iataCode2){
		
		amadeus.shopping.flightDestinations.get({
		
			origin : iataCode1
			
			}).then(function(response){
				// console.log(response.body); // => The raw body
				// console.log(response.result); // => The fully parsed result
				// console.log(response.data); // => The data attribute
					// taken from th
								
				destOne = response.result.dictionaries.locations;
			
			// localStorage.setItem(iataCode1, JSON.stringify(destOne));

			//	DesListOne = JSON.parse(localStorage.getItem(iataCode1));
				
			// console.log(DesListOne);
				
				flightRoutes.setFirstDest(destOne);
				var routeResult = flightRoutes.getRouteInfo();
				
				secondOrigin(res, iataCode1, iataCode2);
				  // result
				}).catch(function(error){
				  console.log(error.response); // => The response object with
												// (un)parsed data
				  console.log(error.response.request); // => The details of
														// the// request// made
			  console.log(error.code); // => A unique error code to identify //
										// the=// type of error
				
			})
	}

	
	function secondOrigin(res, iataCode1, iataCode2){
		//delay(1000);
		amadeus.shopping.flightDestinations.get({
			
			origin: iataCode2
				  
		}).then(function(response){
				 // console.log(response.body); // => The raw body
				// console.log(response.result); // => The fully parsed result
				// console.log(response.data); // => The data attribute taken
							// from the
							
			destTwo = response.result.dictionaries.locations;
			//localStorage.setItem(iataCode2, JSON.stringify(destTwo));
			//DesListTwo = JSON.parse(localStorage.getItem(iataCode2));
			
			flightRoutes.setSecondDest(destTwo);
			
			var SecRouteResult = flightRoutes.getSecRouteInfo();
			compareDestinations(res, iataCode1, iataCode2);
			
		// console.log('******SECOND FLIGHT ROUTE INFO ******: ' +
		// JSON.stringify(SecRouteResult));
				
		}).catch(function(error){
			  console.log(error.response); // => The response object with//
											// (un)parsed data
			 console.log(error.response.request); // => The details of the
													// request made
			 console.log(error.code); // => A unique error code to identify//
										// the// type of error
			}); 
		
		
		
	}
 

	function compareDestinations(res, iataCode1, iataCode2){
		//gets each list of destinations 
		var destList= flightRoutes.getRouteInfo();
		var SecDestList =flightRoutes.getSecRouteInfo();
		
		//from JSON to string
		var JstringOne = JSON.stringify(destList);
		var JstringTwo = JSON.stringify(SecDestList);
		//getting values of object 
		var List = destList.routeDestOne;
		var List2 = SecDestList.routeDestTwo; 
		
		//gets only the keys of both list - eg : MAD, AMS, LON 
		var keySetOne = Object.keys(List);
		var keySetTwo = Object.keys(List2);
		
		//both lists of keys are put in sets 
			var s1= new Set(keySetOne);
			var s2 = new Set(keySetTwo);
			
			 s1.delete(iataCode1);
			 s2.delete(iataCode2);
			
			console.log(s1);
			console.log(s2);
			
		//method with compare both sets and adds any destinations which are in both to a new set
		const set_intersection = (...sets) => sets.reduce(
				 ( A, B) => {
				     var X = new Set();
				     B.forEach( (v => { 
				 if (A.has(v)) X.add(v) }));
				     return X;
				 });
		
		
		var listCompared=set_intersection(s1,s2);
		
		destinationList.setDestList(listCompared);
			
		//	var merged = new Set([...s1, ...s2]);

	//	destinationList.setDestList(listCompared);
		// const waitFor = (ms) => new Promise(r => setTimeout(r, ms));
	 
		 finalDest(res, iataCode1, iataCode2);

	}
	var code1;
	var code1parsed;
	 var code2parsed;
	var code2;
	function finalDest(res, iataCode1, iataCode2){
	
		var finalResult= destinationList.getDestList();
		//console.log('checking code to comapre'+iataCode1);
		
		console.log(finalResult);
		
		 code1= origin.getOriginInfo();
		 code2 = origin.getSecOriginInfo();
		
		code1parsed= code1.routeOriginOne;
		
		console.log(finalResult);
		//console.log('code 1 parsed '+code1parsed);
		
		var DestinationL; 
		var destLoop = finalResult.destinationList;
	

		var i, destArray =[];
		destArray= Array.from(destLoop);
		console.log(JSON.stringify(destArray));
		//var DestResult =JSON.stringify([...destLoop]);

		var intervalID= setInterval(function(){	
		},2000);
		
		
		for( let i=0, len=destArray.length; i<len; i++){
			setTimeout(()=> {
				
				DestinationL= destArray[i];
				//console.log(destArray[i]);
				
				
				findPrices(res, code1parsed, DestinationL);
				
				
				if (i === len){
					setTimeout(() => {
						
					}, 1000);
				}
			},i * 1000);
		}

		//finalDest2(res, iataCode1, iataCode2)
	}
/*	 function finalDest2(res, iataCode1, iataCode2) {

		 var finalResult2 = destinationList.getDestList();
		 //console.log('checking code to comapre'+iataCode1);

		 console.log(finalResult2);

		 code1 = origin.getOriginInfo();
		 code2 = origin.getSecOriginInfo();

		 code1parsed = code1.routeOriginOne;
		 code2parsed = code2.routeOriginTwo;

		 //console.log(finalResult);
		 //console.log('code 1 parsed '+code1parsed);

		 var DestinationL;
		 var destLoop = finalResult2.destinationList;


		 var i, destArray = [];
		 destArray = Array.from(destLoop);
		 console.log(JSON.stringify(destArray));
		 //var DestResult =JSON.stringify([...destLoop]);

		 // var intervalID= setInterval(function(){
		 // },2000);

		 console.log('before loop 2nd dest');

		 for (let i = 0, p = Promise.resolve(), len = destArray.length; i < len; i++) {


				 DestinationL = destArray[i];

					 console.log('in promise loop');

					 p = p.then(_ => new Promise(resolve =>
						 setTimeout(() => {
							 findPrices2(res, code2parsed, DestinationL);
							 console.log('during 2nd dest loop ' + code2parsed);

							 resolve();

						 }, 1000))
					 )

				 }


				 if (i === len) {
					 setTimeout(() => {

					 }, 1000);
				 }
			 }, i * 1000);
}
		 }
	 }*/
	var prices;
	var priceSearchVar;
	 var priceSearchVar2;
	var storePriceVar;
	 var storePriceVar2;
	var res2;
	var priceRes;
			

	 function findPrices(res, A, B){
		 (amadeus.shopping.flightOffers.get({
			 origin: A,
			 destination : B,
			 departureDate:"2019-09-10"

		 }).then(function(response){
			 // console.log(response.body); // => The raw body
			 priceSearchVar = JSON.stringify(response.result.data);

			 destinationList.setPriceSearch1(priceSearchVar);
			 storePriceVar= destinationList.getPriceSearch1();



			 //postPrices(res, storePriceVar);
			 fs.writeFileSync('api/data/destinations1.json', priceSearchVar)

			 //	console.log(priceSearchVar);
			 // app.set('data', storePriceVar);
			 //localStorage.setItem('prices', JSON.stringify(storePriceVar));
			 //  postPrices(res,A,B);

		 }).catch(function(error){
			 console.log(error.response); // => The response object with//
			 // (un)parsed data
			 console.log(error.response.request); // => The details of the
			 // request made
			 console.log(error.code); // => A unique error code to identify//
			 // the// type of error
		 }))

	 }

	/* function findPrices2(res, A, B){
		 (amadeus.shopping.flightOffers.get({
			 origin: A,
			 destination : B,
			 departureDate:"2019-10-10"

		 }).then(function(response){
			 // console.log(response.body); // => The raw body
			 priceSearchVar2 = JSON.stringify(response.result.data);

			 destinationList.setPriceSearch2(priceSearchVar2);
			 storePriceVar2= destinationList.getPriceSearch2();
			 console.log(storePriceVar2);
			 //postPrices(res, storePriceVar);
			 fs.writeFileSync('api/data/destinations2.json', priceSearchVar2)
			 //	console.log(priceSearchVar);
			 // app.set('data', storePriceVar);
			// localStorage.setItem('prices', JSON.stringify(storePriceVar2));
			 //  postPrices(res,A,B);

		 }).catch(function(error){
			 console.log(error.response); // => The response object with//
			 // (un)parsed data
			 console.log(error.response.request); // => The details of the
			 // request made
			 console.log(error.code); // => A unique error code to identify//
			 // the// type of error
		 }))

	 }
	 /*function postPrices(res, A, B){
          res2 = A;
         // console.log(JSON.parse(res2));
         console.log(res2);
         res.send(res2)

     };*/
	
	});
    
    

//app.get('/search', function(req,res ){
 //res.render('search');
//})  

		
 app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/'));
 
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


const middleware = [

  express.static(path.join(__dirname, 'public')),
]

app.use(middleware);
app.use('/', routes);
app.use('/search', routes);


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(require('express-jquery')('/jquery.js'));






// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
 
