import React from 'react';
import { connect } from 'react-redux';

import { setLocationTextInputOriginOne, setLocationTextInputOriginTwo, setDefaultInput } from '../../store/actions/index';

//class Home extends Component {

//const Home = () => {

         //  return (

 export class Home extends React.Component {

  constructor(props) {
    super(props);

    this.props.actions.setDefaultInput();
    this.inputFieldRef = React.createRef();
    this.inputFieldRef2 = React.createRef();
  }


  handleButtonClick = (event) => {
    this.props.actions.setLocationTextInputOriginOne({ locationData: this.inputFieldRef.current.value });
    this.props.actions.setLocationTextInputOriginTwo({ locationData2: this.inputFieldRef2.current.value })
  }

  componentDidUpdate = (prevProps) => {
    const { locationData, locationData2} = this.props;

    if (prevProps.locationData!== locationData && prevProps.locationData2 !== locationData2 ) {
      this.props.history.push({
        pathname: '/',
        state: {
         locationData2: locationData2,
          locationData: locationData,
        },
        
      });
    }
  };

  render() {
    return (           
               
<form name="FlightForm" id="FlightForm"  method="POST" action="/search-destinations">
      <h3>Organise your holiday with Tripee</h3>
      <div >
        <div className="w3-half">
          <label>FROM</label>
         <input ref={this.inputFieldRef} type='text' name='originOne' id='originOne' className="w3-input w3-border" placeholder="Departing from"/>
        </div>
        <div className="w3-half">
          <label>FROM</label>
          <input ref={this.inputFieldRef2} name='originTwo' id='originTwo' className="w3-input w3-border" type="text" placeholder="Departing from"/>
        </div>
      </div>
      <p><button onClick={this.handleButtonClick} type="submit" name="search"  id="search" className="w3-button w3-dark-grey">Search and find dates</button></p>

</form>
)};
    }

const mapStateToProps = function(state) {
  const locationData = state && state.locationData;
  const locationData2 = state && state.locationData2;


  return {

    locationData: locationData,
    locationData2 : locationData2,
  };
};

const mapDispatchToProps = function(dispatch) {
  const dispatchActions = {
   
    setLocationTextInputOriginOne: function(locationInputConfigOne) {
      dispatch(setLocationTextInputOriginOne(locationInputConfigOne));
    },
    setLocationTextInputOriginTwo: function(locationInputConfigTwo) {
      dispatch(setLocationTextInputOriginTwo(locationInputConfigTwo));
    },
  
   
  };

  return {
    actions: dispatchActions,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);

    
