import React from 'react';
import {DestinationPrices} from '../containers/DestinationPrices';

const App = ({ children }) => (
  <main>
    {children}
    <DestinationPrices/>
  </main>
);

export default App;
