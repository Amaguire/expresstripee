import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import { Provider } from 'react-redux';

// Store
import store from './store/index';


// display views 
import {App} from './views/components/index';
import {Home} from './views/containers/index';



ReactDOM.render(
  <Provider store={store}>
<Router>
<App>
<Route exact path ='/' component ={Home}/>

</App>
</Router>
</Provider>
, document.getElementById('root'));


