import { SET_LOCATION_TEXT_INPUT_ORIGIN_ONE, SET_LOCATION_TEXT_INPUT_ORIGIN_TWO, SET_DEFAULT_INPUT,
  GET_FLIGHT_DATA, LOADING_FLIGHT_DATA, LOADED_FLIGHT_DATA, ERROR_FLIGHT_DATA } from './action-types';

export const setLocationTextInputOriginOne = locationInputConfigOne => (
  {
    type: SET_LOCATION_TEXT_INPUT_ORIGIN_ONE,
    payload: locationInputConfigOne,
  }
);
export const setLocationTextInputOriginTwo = locationInputConfigTwo => (
  {
    type: SET_LOCATION_TEXT_INPUT_ORIGIN_TWO,
    payload: locationInputConfigTwo,
  }
);



export const setDefaultInput = () => {
  return {
    type: SET_DEFAULT_INPUT,
  };
};

export const getFlightData = (flightSearchData) => {
  return {
    type: GET_FLIGHT_DATA,
    payload: flightSearchData,
  };
};

export { SET_LOCATION_TEXT_INPUT_ORIGIN_ONE,SET_LOCATION_TEXT_INPUT_ORIGIN_TWO, SET_DEFAULT_INPUT,
  GET_FLIGHT_DATA, LOADING_FLIGHT_DATA, LOADED_FLIGHT_DATA, ERROR_FLIGHT_DATA };
