import { put, call, takeLatest } from 'redux-saga/effects';

import * as actions from '../actions/action-types';
import { TripeeAppAPI } from '../../utils';

export default function * root() {
  yield takeLatest(actions.GET_FLIGHT_DATA, fetchFlightData);
};

function * fetchFlightData(action) {
  try {
    const { flightSearchData } = action.payload;

    yield put({ type: actions.LOADING_FLIGHT_DATA });

    const response = yield call(flightApiPromiseWrapper, flightSearchData);

    yield put({ type: actions.LOADED_FLIGHT_DATA, payload: { ...response.data } });

  } catch (e) {
    yield put({type: actions.ERROR_FLIGHT_DATA });
  }
}

function * flightApiPromiseWrapper(flightSearchData) {
  const promise = yield new Promise((resolve, reject) => {
    TripeeAppAPI({}, flightSearchData, function(err, data) {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });

  return promise;
}
