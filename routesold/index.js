var express = require('express');
var router = express.Router();
var destinationList = require('dest-list-tripee');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Tripee' });
});


//this routes to search page for results

router.get('/search', function(req, res, next) {
	
	  res.render('search', { title: 'Tripee' });

	});



module.exports = router;



